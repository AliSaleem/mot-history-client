<?php

namespace AliSaleem\MOTHistory\Models;

use DateTime;
use stdClass;
use function date_create_from_format;
use function usort;

/**
 * @property string                                 $vrn
 * @property string                                 $make
 * @property string                                 $model
 * @property DateTime                               $first_used_date
 * @property string                                 $fuel
 * @property string                                 $colour
 * @property \AliSaleem\MOTHistory\Models\MOTTest[] $tests
 * @property DateTime                               $expiry_date
 * @property DateTime                               $last_test_date
 */
class MOTHistory extends AbstractModel
{
    public function __construct(stdClass $motHistoryObject)
    {
        $this->vrn = $motHistoryObject->registration ?? null;
        $this->make = $motHistoryObject->make ?? null;
        $this->model = $motHistoryObject->model ?? null;
        $this->first_used_date = isset($motHistoryObject->firstUsedDate)
            ? date_create_from_format('Y.m.d', $motHistoryObject->firstUsedDate)
            : null;
        $this->fuel = $motHistoryObject->fuelType ?? null;
        $this->colour = $motHistoryObject->primaryColour ?? null;

        $tests = [];
        foreach ($motHistoryObject->motTests ?? [] as $apiTest) {
            $tests[] = new MOTTest($apiTest);
        }
        usort($tests, function (MOTTest $a, MOTTest $b) {
            return $b->completed_date <=> $a->completed_date;
        });
        $this->tests = $tests;

        if (isset($motHistoryObject->motTestDueDate)) {
            $this->expiry_date = date_create_from_format('Y.m.d', $motHistoryObject->motTestDueDate);
        } else {
            foreach ($this->tests as $test) {
                if ($test->expiry_date) {
                    $this->expiry_date = $test->expiry_date;
                    break;
                }
            }
        }
        foreach($this->tests as $test){
            if($test->completed_date){
                $this->last_test_date = $test->completed_date;
                break;
            }
        }
    }
}
