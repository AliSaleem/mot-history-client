<?php

namespace AliSaleem\MOTHistory\Models;

use JsonSerializable;

use function array_diff;
use function array_key_exists;
use function array_keys;

abstract class AbstractModel implements JsonSerializable
{
    /** @var array */
    private $attributes = [];
    /** @var array */
    private $hidden = [];

    public function __get($name)
    {
        if (!array_key_exists($name, $this->attributes)) {
            return null;
        }

        return $this->attributes[$name];
    }

    public function __set($name, $value): void
    {
        $this->attributes[$name] = $value;
    }

    public function __isset($name): bool
    {
        return array_key_exists($name, $this->attributes);
    }

    public function jsonSerialize(): array
    {
        $publicAttributeKeys = array_diff(array_keys($this->attributes), $this->hidden);

        $publicAttributes = [];
        foreach ($publicAttributeKeys as $key) {
            $publicAttributes[$key] = $this->attributes[$key];
        }

        return $publicAttributes;
    }
}
