<?php

namespace AliSaleem\MOTHistory\Models;

use DateTime;
use stdClass;
use function date_create_from_format;
use function strtolower;
use function ucwords;
use function usort;

/**
 * @property DateTime                                      $completed_date
 * @property string                                        $result
 * @property DateTime                                      $expiry_date
 * @property integer                                       $odometer
 * @property string                                        $odometer_unit
 * @property string                                        $odometer_result_type
 * @property string                                        $test_number
 * @property \AliSaleem\MOTHistory\Models\MOTTestComment[] $comments
 */
class MOTTest extends AbstractModel
{
    public function __construct(stdClass $motTestObject)
    {
        $this->completed_date = isset($motTestObject->completedDate)
            ? date_create_from_format('Y.m.d H:i:s', $motTestObject->completedDate)
            : null;
        $this->result = ucwords(strtolower($motTestObject->testResult));
        $this->expiry_date = isset($motTestObject->expiryDate)
            ? date_create_from_format('Y.m.d', $motTestObject->expiryDate)
            : null;
        $this->odometer = intval($motTestObject->odometerValue);
        $this->odometer_unit = $motTestObject->odometerUnit ?? 'unknown';
        $this->odometer_result_type = $motTestObject->odometerResultType;
        $this->test_number = $motTestObject->motTestNumber;

        $comments = [];
        foreach ($motTestObject->rfrAndComments as $comment) {
            $comments[] = new MOTTestComment($comment);
        };
        usort($comments, function (MOTTestComment $a, MOTTestComment $b) {
            return $a->type <=> $b->type;
        });
        $this->comments = $comments;
    }

    public function hasPassed()
    {
        return $this->result === 'Passed';
    }
}
