<?php

namespace AliSaleem\MOTHistory\Models;

use stdClass;
use function strtolower;

/**
 * @property string  $text
 * @property string  $type
 * @property boolean $dangerous
 */
class MOTTestComment extends AbstractModel
{
    public function __construct(stdClass $motTestCommentObject)
    {
        $this->text = $motTestCommentObject->text;
        $this->type = $motTestCommentObject->type;
        $this->dangerous = $motTestCommentObject->dangerous || strtolower($this->type) === 'dangerous';
    }
}
