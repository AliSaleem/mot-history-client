<?php

namespace AliSaleem\MOTHistory;

use AliSaleem\Curl\Request;
use AliSaleem\MOTHistory\Exceptions\HistoryNotFoundException;
use AliSaleem\MOTHistory\Models\MOTHistory;
use InvalidArgumentException;
use function count;
use function is_array;
use function is_null;
use function preg_match;
use function preg_replace;
use function reset;
use function strtoupper;

class Client
{
    const BASE_URL = 'https://beta.check-mot.service.gov.uk/trade/vehicles/mot-tests';

    const VRN_REGEX = '/(^[A-Z]{2}[0-9]{2}\s?[A-Z]{3}$)|(^[A-Z][0-9]{1,3}\s?[A-Z]{3}$)|(^[A-Z]{3}\s?[0-9]{1,3}[A-Z]$)|(^[0-9]{1,4}\s?[A-Z]{1,2}$)|(^[0-9]{1,3}\s?[A-Z]{1,3}$)|(^[A-Z]{1,2}\s?[0-9]{1,4}$)|(^[A-Z]{1,3}\s?[0-9]{1,3}$)/';

    /** @var string */
    private $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    protected function makeRequest()
    {
        return (new Request(self::BASE_URL))
            ->setHeaders([
                'Accept'    => 'application/json+v4',
                'x-api-key' => $this->apiKey,
            ]);
    }

    public function getHistory(string $vrn): ?MOTHistory
    {
        $vrn = strtoupper(preg_replace('/\s/', '', $vrn));
//        if (!preg_match(self::VRN_REGEX, $vrn)) {
//            throw new InvalidArgumentException('The VRN provided is invalid');
//        }
        $history = $this->makeRequest()
            ->addParameter('registration', $vrn)
            ->get()
            ->getBodyAsObject();
        if (!is_array($history) or count($history) !== 1) {
            return null;
        }

        return new MOTHistory(reset($history));
    }

    protected function getHistoryOrFail(string $vrn): MOTHistory
    {
        $history = $this->getHistory($vrn);
        if (is_null($history)) {
            throw new HistoryNotFoundException($vrn);
        }

        return $history;
    }
}
