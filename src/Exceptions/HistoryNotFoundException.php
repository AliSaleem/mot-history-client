<?php

namespace AliSaleem\MOTHistory\Exceptions;

use Exception;
use Throwable;

class HistoryNotFoundException extends Exception
{
    public function __construct(string $vrn = '', int $code = 0, Throwable $previous = null)
    {
        $message = $vrn
            ? 'MOT History for ' . $vrn . ' could not be found'
            : 'MOT History could not be found';

        parent::__construct($message, $code, $previous);
    }
}
